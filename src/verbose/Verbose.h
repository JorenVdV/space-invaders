/*
 * Verbose.h
 *
 *  Created on: Nov 19, 2013
 *      Author: EduardusdeMul
 */

#pragma once
#ifndef VERBOSE_H_
#define VERBOSE__H_

#include <stack>
#include <string>
#include <iostream>
#include <sstream>
#include <utility>
#include <algorithm>
#include <stdlib.h>
#include "../DesignByContract.h"
#include "../Color/ccolor.h"

namespace mnb {

class Verbose {
public:

	/*
	 * a function to  initialize the verbose object, this makes sure that we use just one verbose object
	 * @param bool verbose, should verbose be on of off
	 * @post should be properly initialized
	 */
	static Verbose* instance(bool verbose);

	/*
	 * the standard constructor for the verbose class
	 */
	virtual ~Verbose();

	/*
	 * add's a function to the output, and sets the indentation Level one higher
	 * @param string functionName the name of the function with parameters.
	 * @pre the functionName may not be empty
	 * @post the indentation level should be increased
	 */
	void addFunction(std::string functionName);

	/*
	 * a functions that prints output that the current function is finished, and sets the indentation Level to the previous level.
	 * @post the indentation level should be decreased
	 */
	void closeCurrentFunction();

	/*
	 * a function to put a random message to the verbose stream
	 * @param string messages the message that should be added to the stream.
	 * @pre the messages may not be empty
	 */
	void comment(std::string message);

	/*
	 * a function to put a random error message to the verbose stream
	 * @param const char* err the error message that should be added to the stream.
	 * @pre the messages may not be empty
	 */
	void error(const char* err);

	/*
	 * a function to put a random error message to the verbose stream
	 * @param std::string err the error message that should be added to the stream.
	 * @pre the messages may not be empty
	 */
	void error(std::string err);

	/*
	 * the overloaded function for the post increment
	 * @param int the base value
	 * @post the indentation level should be increased
	 */
	Verbose operator++(int){
		Verbose original = *this;
		int t_indentaionLevel = _indentationLevel;
		_indentationLevel++;
		ENSURE(_indentationLevel > t_indentaionLevel, "the indentation level is not increased");
		return original;

	}

	/*
	 * the overloaded function for the post decrement
	 * @param  int the base value
	 * @post the indentation level should be decreased
	 */
	Verbose operator--(int){
		Verbose original = *this;
		int t_indentaionLevel = _indentationLevel;
		_indentationLevel--;
		ENSURE(_indentationLevel < t_indentaionLevel, "the indentation level is not decreased");
		return original;
	}

	/*
	 * a function to checks if the mode is verbose or not
	 */
	bool isVerbose() const{
		return _verbose;
	}


private:

	/*
		 * the constructor for the verbose class, to create a verbose object.
		 * @param bool verbose, if true than there should be output, otherwise no output
		 * @post the object should be properly initialized
		 */
		Verbose(bool verbose);

	/*
	 * function to create the right indentation
	 * @pre the message that we have to make can not be empty
	 */
	void  generatingIndentation();

	/*
	 * function to print all the messages
	 * @pre the message that we have to make can not be empty
	 */
	void print();


	static Verbose* _instance;
	bool _verbose;
	std::stack< std::pair<std::string, int> > _currentFuction;
	int _indentationLevel;

	std::string  _message;
	bool _error;

};



} /* namespace mnb */
#endif /* VERBOSE__H_ */
