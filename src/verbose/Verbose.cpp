/*
 * Verbose.cpp
 *
 *  Created on: Nov 19, 2013
 *      Author: EduardusdeMul
 */

#include "Verbose.h"

namespace mnb {

Verbose* Verbose::_instance = NULL;

Verbose* Verbose::instance(bool verbose){
	if(_instance == NULL){
		_instance = new Verbose(verbose);
	}
	return _instance;
}

Verbose::~Verbose() {
	// TODO Auto-generated destructor stub
}

void Verbose::addFunction(std::string functionName){
	functionName.erase(remove_if(functionName.begin(), functionName.end(), isspace), functionName.end());
	REQUIRE(!functionName.empty(), "FunctionName may not be empty");
	int t_indentaionLevel = _indentationLevel;
	_currentFuction.push(std::make_pair(functionName, _indentationLevel));
	_message.append("Begin function: ");
	_message.append(functionName);
	this->print();
	_indentationLevel++;
	ENSURE(_indentationLevel > t_indentaionLevel, "the indentation level is not increased, while a new function is created");
}

void Verbose::closeCurrentFunction(){
	int t_indentaionLevel = _indentationLevel;
	_indentationLevel = _currentFuction.top().second;
	_message.append("ending function: ");
	_message.append(_currentFuction.top().first);
	_currentFuction.pop();
	this->print();
	ENSURE(_indentationLevel < t_indentaionLevel, "the indentation level is not decreased, while a function is ended");
}

void Verbose::comment(std::string message){
	//message.erase(remove_if(message.begin(), message.end(), isspace), message.end());
	REQUIRE(!message.empty(), "the messages for the commit may not be empty");
	_message.append(message);
	this->print();
}

void Verbose::error(const char* err){
	//	REQUIRE(err != NULL, "the error message for the commit may not be empty");
	_message.append("Something went wrong: ");
	_message.append(err);
	_error = true;
	this->print();
}

void Verbose::error(std::string err){
	REQUIRE(!err.empty(), "the error message for the commit may not be empty");
	_message.append("Something went wrong: ");
	_message.append(err);
	_error = true;
	this->print();
}

Verbose::Verbose(bool verbose) :_verbose(verbose), _indentationLevel(0), _message(""), _error(false) {
}


void Verbose::generatingIndentation(){
	REQUIRE(!_message.empty(), "the message that we have to make can not be empty");
	std::string str;
	if(!_error){
		for(int i = 0; i < _indentationLevel; i++ ){
			str.append("--");
		}
	}
	else{
		for(int i = 0; i < _indentationLevel; i++ ){
			str.append("!!");
		}
	}
	str.append(_message);
	_message = str;
	str.clear();
}

void Verbose::print(){
	REQUIRE(!_message.empty(), "the message that we have to make can not be empty");
	if(_verbose){
		generatingIndentation();
		if(!_error){
			std::cout<<zkr::cc::fore::lightgreen<<_message<<std::endl;
		}else{
			std::cout<<zkr::cc::fore::lightred<<_message<<std::endl;
		}
	}
	std::cout<<zkr::cc::fore::console;
	_error = false;
	_message.clear();
}

} /* namespace mnb */
